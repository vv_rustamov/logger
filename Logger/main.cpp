/* 
 * File:   main.cpp
 * Author: vadim
 *
 * Created on October 11, 2014, 1:40 PM
 */

#include <cstdlib>
#include "src/h/Logger.h"
#include "src/h/LogConfig.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    LogConfig* log_configure;
    log_configure.SetMinimalLoggingLevel(PriorityLevel.error);
    log_configure.AddLoggerReceiver();
    Logger* log = new Logger(log_configure);
    return 0;
}

