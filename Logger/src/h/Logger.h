/* 
 * File:   Log.h
 * Author: vadim
 *
 * Created on October 11, 2014, 1:51 PM
 */
#ifndef LOGGER_H
#define	LOGGER_H

#include "LogConfig.h"
#include "PriorityLevel.h"
#include <string>

class Logger {
public:
    /* void LoggerConfigure(const LoggerConfig& )
     * Конфигурирование начальных настроек логгера.
     * @params: 
     * @input: _logConfig - объект конфигурации логгера
     * @output: void
     */
    void LoggerConfigure(const LogConfig& _logConfig);
    
    /* void ChangeLoggingLevel(const PriorityLevel& _minPriorityLoggingLevel)
     * В любой момент работы логгера можно управлять его уровнем логирования.
     * По умолчанию, уровень логирования самый полный (логирует сообщения 
     * любых приоритетов).
     * @params: 
     * @input: _minPriorityLoggingLevel - установка минимального 
     *                                    приоритета логгера.
     * @output: void   
     */
    void ChangeLoggingLevel(const PriorityLevel& _minPriorityLoggingLevel);
    void appendMsg(const PriorityLevel& _lvl, const std::string& _msg);
    void appendMsg(const PriorityLevel& _lvl, const std::string& _msg,
                    const bool& doPrintTime);
    Logger(const Logger& orig);
    virtual ~Logger();
private:
    Logger();

};

#endif	/* LOGGER_H */

