/* 
 * File:   LogConfig.h
 * Author: vadim
 *
 * Created on October 11, 2014, 2:24 PM
 */

#ifndef LOGCONFIG_H
#define	LOGCONFIG_H

#include "Logger.h"


class LogConfig {
public:
    LogConfig();
    LogConfig(const LogConfig& orig);
    void SetMinimalLoggingLevel(const PriorityLevel& _minimalLogLevel);
    void AddLoggerReceiver(LogReceiver& _receiverObject);
    void RemoveLoggerReceiver(LogReceiver& _receiverObject);
    virtual ~LogConfig();
private:

};

#endif	/* LOGCONFIG_H */

