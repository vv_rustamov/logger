/* 
 * File:   PriorityLevel.h
 * Author: vadim
 *
 * Created on October 12, 2014, 7:05 PM
 */

#ifndef PRIORITYLEVEL_H
#define	PRIORITYLEVEL_H

enum PriorityLevel{
    info,
    debug,
    warn,
    error,
    fatal
};

#endif	/* PRIORITYLEVEL_H */

