/* 
 * File:   LogReceiver.h
 * Author: vadim
 *
 * Created on October 12, 2014, 12:35 PM
 */

#ifndef LOGRECEIVER_H
#define	LOGRECEIVER_H
#include "Message.h"

class LogReceiver {
public:
    LogReceiver();
    LogReceiver(const LogReceiver& orig);
    std::string& GetReceiverName(void) const;
    void SetReceiverName(const std::string& _name);
    virtual void AppendMessage(const Message&);
    virtual ~LogReceiver();
private:
};

#endif	/* LOGRECEIVER_H */

