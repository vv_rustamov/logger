/* 
 * File:   Message.h
 * Author: vadim
 *
 * Created on October 12, 2014, 1:09 PM
 */

#ifndef MESSAGE_H
#define	MESSAGE_H

class Message {
public:
    Message();
    Message(const Message& orig);
    virtual ~Message();
private:

};

#endif	/* MESSAGE_H */

