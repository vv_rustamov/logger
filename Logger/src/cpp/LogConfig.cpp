/* 
 * File:   LogConfig.cpp
 * Author: vadim
 * 
 * Created on October 11, 2014, 2:24 PM
 */

#include "../h/LogConfig.h"
#include "../h/PriorityLevel.h"
#include <set>
LogConfig::LogConfig() {
}

LogConfig::LogConfig(const LogConfig& orig) {
}

LogConfig::~LogConfig() {
}

PriorityLevel* minimalPrioritet;

LogConfig::SetMinimalLoggingLevel(const PriorityLevel& _minimalLogLevel){
    minimalPrioritet = _minimalLogLevel;
}
std::set<LogReceiver&> receiverSet;
LogConfig::AddLoggerReceiver(LogReceiver& _reciver){
    receiverSet.insert(_reciver);
}

LogConfig::RemoveLoggerReceiver(LogReceiver& _receiverObject){
    receiverSet.erase(_receiverObject);
}